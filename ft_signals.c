/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signals.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 16:39:45 by arangari          #+#    #+#             */
/*   Updated: 2018/05/23 16:46:08 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void					ft_sigwinch(int sig)
{
	struct winsize		ts;

	if (sig == SIGWINCH)
	{
		ioctl(0, TIOCGWINSZ, &ts);
		display_list(set_list(NULL, 'g'));
		signal(SIGWINCH, ft_sigwinch);
	}
}

void					sighandlercnt(int sig)
{
	if (sig == SIGCONT)
	{
		init_config();
		display_list(set_list(NULL, 'g'));
	}
}

void					sighandler(int sig)
{
	if (sig == SIGTSTP)
	{
		signal(SIGTSTP, SIG_DFL);
		signal(SIGCONT, sighandlercnt);
		kill(getpid(), SIGTSTP);
	}
}

void					ft_id(t_list **hd)
{
	t_list				*current;

	current = *hd;
	ft_update_id(hd);
	free(current);
}

t_list					*ft_space(t_list *hd, int max, char b[0])
{
	if (b[0] == SPACE)
		active_node(&hd, line_no(1, 'g'), max);
	else if (b[0] == BACK_SPC)
		ft_delete_list(&hd, line_no(1, 'g'), max);
	else if (b[0] == DELETE)
		ft_delete_list(&hd, line_no(1, 'g'), max);
	else if (b[0] == ENTER)
		ft_exit(10, hd);
	return (hd);
}
