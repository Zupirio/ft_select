/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_global.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:11:09 by arangari          #+#    #+#             */
/*   Updated: 2018/05/23 17:32:11 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int							line_no(int val, char type)
{
	static int				line_no;

	if (type == 's')
		line_no = val;
	else if (type == 'g')
		return (line_no);
	return (0);
}

int							ft_max_length(int val, char type)
{
	static unsigned int		max_length;

	if (type == 's')
		max_length = val;
	else if (type == 'g')
		return (max_length);
	return (0);
}

struct termios				*default_info(struct termios *info, char type)
{
	static struct termios def_info;

	if (type == 's')
		def_info = *info;
	else if (type == 'g')
		return (&def_info);
	return (0);
}

struct termios				*set_info(struct termios *info, char type)
{
	static struct termios	def_info;

	if (type == 's')
		def_info = *info;
	else if (type == 'g')
		return (&def_info);
	return (0);
}

t_list						*set_list(t_list **hd, char type)
{
	static t_list *s_hd;

	if (type == 's')
		s_hd = *hd;
	else if (type == 'g')
		return (s_hd);
	return (NULL);
}
