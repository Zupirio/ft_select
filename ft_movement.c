/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_movement.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:11:12 by arangari          #+#    #+#             */
/*   Updated: 2018/05/23 17:25:54 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void					ft_sp(void)
{
	struct termios			*sp;

	sp = set_info(NULL, 'g');
	sp->c_cc[VTIME] = 0;
	sp->c_cc[VMIN] = 1;
}

static void					ft_st(void)
{
	struct termios			*sp;

	sp = set_info(NULL, 'g');
	sp->c_cc[VTIME] = 0;
	sp->c_cc[VMIN] = 1;
}

int							ft_keys(char b[0], int max, int line)
{
	if (b[0] == RIGHT_BTN && line < max)
	{
		tputs(tgetstr("nd", NULL), 1, ft_putchar_fd);
		line++;
	}
	else if (b[0] == LEFT_BTN && line > 0 && line <= max)
	{
		tputs(tgetstr("le", NULL), 1, ft_putchar_fd);
		if (line != 1)
			line--;
		else
			line = max;
	}
	else if (line == max)
		line = 1;
	else if (line == 0)
		line = max;
	return (line);
}

int							ft_movement(int max)
{
	char				b[1];
	int					line;
	struct termios		*sp;

	line = line_no(1, 'g');
	sp = set_info(NULL, 'g');
	ft_st();
	tcsetattr(0, TCSANOW, sp);
	read(0, b, 1);
	if (b[0] == '[')
	{
		read(0, b, 1);
		line = ft_keys(&b[0], max, line);
		line_no(line, 's');
		ft_sp();
		tcsetattr(0, TCSANOW, sp);
	}
	else
		ft_exit(0, NULL);
	return (1);
}
