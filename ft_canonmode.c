/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_canonmode.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:11:04 by arangari          #+#    #+#             */
/*   Updated: 2018/01/26 13:02:10 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int						ft_putchar_fd(int n)
{
	write(2, &n, 1);
	return (1);
}

size_t					ft_strlen(const char *c)
{
	int					len;

	len = 0;
	while (c[len])
		len++;
	return (len);
}

void					ft_putstr_fd(char *str)
{
	while (*str)
		ft_putchar_fd(*str++);
}

void					init_config(void)
{
	char				*term_type;
	struct termios		tp;
	struct termios		sp;

	term_type = getenv("TERM");
	tgetent(NULL, term_type);
	tcgetattr(0, &tp);
	sp = tp;
	default_info(&tp, 's');
	sp.c_lflag &= ~(ICANON | ECHO);
	sp.c_cc[VTIME] = 0;
	sp.c_cc[VMIN] = 1;
	set_info(&sp, 's');
	tcsetattr(0, TCSANOW, &sp);
	tputs(tgetstr("cl", NULL), 1, ft_putchar_fd);
}
