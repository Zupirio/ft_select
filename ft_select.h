/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:11:14 by arangari          #+#    #+#             */
/*   Updated: 2018/06/28 18:00:44 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/ioctl.h>
# include <termios.h>
# include <stdbool.h>
# include <term.h>
# include <curses.h>
# include <signal.h>
# include <string.h>
# define ESCAPE 27
# define SPACE 32
# define UP_BTN 'A'
# define DOWN_BTN 'B'
# define RIGHT_BTN 'C'
# define LEFT_BTN 'D'
# define BACK_SPC 8
# define DELETE 127
# define ENTER 10

typedef struct			s_list
{
	int					id;
	char				*name;
	bool				u_line;
	bool				active;
	struct s_list		*next;
}						t_list;

void					ft_create_list(char **av, t_list **hd);
void					init_config(void);
int						ft_putchar_fd(int n);
void					ft_putstr_fd(char *str);
int						line_no(int val, char type);
struct termios			*default_info(struct termios *info, char type);
struct termios			*set_info(struct termios *info, char type);
t_list					*set_list(t_list **hd, char type);
int						ft_movement(int max);
int						display_list(t_list *hd);
int						active_node(t_list **hd, int line_no, int max);
void					ft_update_id(t_list **hd);
void					ft_delete_list(t_list **hd, int line, int max);
void					ft_exit(int n, t_list *hd);
size_t					ft_strlen(const char *c);
void					stop_signal(void);
void					signal_handler(int signo);
void					handle_sigwinch(int sig);
int						ft_max_length(int val, char type);
void					ft_sigwinch(int sig);
int						display_list_horizontal(t_list *hd);
void					sighandler(int sig);
void					ft_id(t_list **hd);
t_list					*ft_space(t_list *hd, int max, char b[0]);

#endif
