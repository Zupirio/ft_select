/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lists.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:11:10 by arangari          #+#    #+#             */
/*   Updated: 2018/05/23 17:32:44 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_list			*ft_new_node(char *name, int id)
{
	t_list		*new_node;

	if (!(new_node = (t_list *)malloc(sizeof(t_list))))
		return (NULL);
	new_node->name = name;
	new_node->id = id;
	new_node->active = false;
	new_node->u_line = false;
	new_node->next = NULL;
	return (new_node);
}

void			ft_add_list(t_list **hd, t_list *new_node)
{
	t_list		*p_list;

	p_list = *hd;
	if (!p_list)
		(*hd) = new_node;
	else
	{
		while (p_list->next)
			p_list = p_list->next;
		p_list->next = new_node;
	}
}

void			ft_create_list(char **av, t_list **hd)
{
	int					id;
	unsigned int		max_length;

	id = 1;
	max_length = 0;
	while (av && *(av + 1))
	{
		if (max_length < ft_strlen(*(av + 1)))
			max_length = ft_strlen(*(av + 1));
		ft_add_list(hd, ft_new_node(*(av + 1), id++));
		av = av + 1;
	}
	ft_max_length(max_length, 's');
}

void			ft_update_id(t_list **hd)
{
	t_list		*p_hd;
	int			count;

	count = 1;
	p_hd = *hd;
	while (p_hd)
	{
		p_hd->id = count++;
		p_hd = p_hd->next;
	}
}

void			ft_delete_list(t_list **hd, int line, int max)
{
	t_list		*current;
	t_list		*previous;

	current = *hd;
	while (current != NULL && current->id == line)
	{
		*hd = current->next;
		if (max == 1)
			ft_exit(0, NULL);
		ft_id(hd);
		return ;
	}
	while (current != NULL && current->id != line)
	{
		previous = current;
		current = current->next;
	}
	if (current == NULL)
		return ;
	previous->next = current->next;
	if (line < max && line != 0)
		ft_update_id(hd);
	if (line == max)
		line_no(line - 1, 's');
	free(current);
}
