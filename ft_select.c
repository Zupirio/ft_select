/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:11:13 by arangari          #+#    #+#             */
/*   Updated: 2018/06/29 08:52:51 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void						ft_display_output(t_list *hd)
{
	while (hd)
	{
		if (hd->active)
			printf("%s", hd->name);
		if (hd->next)
			printf(" ");
		else
			printf("\n");
		hd = hd->next;
	}
}

void						ft_exit(int n, t_list *hd)
{
	tputs(tgetstr("ve", NULL), 1, ft_putchar_fd);
	tputs(tgetstr("cl", NULL), 1, ft_putchar_fd);
	tputs(tgetstr("", NULL), 1, ft_putchar_fd);
	tcsetattr(0, TCSANOW, default_info(NULL, 'g'));
	if (n == 10 && hd)
		ft_display_output(hd);
	exit(EXIT_SUCCESS);
}

static void					ft_key(int max, char b[1])
{
	if (b[0] == ESCAPE)
	{
		if (!ft_movement(max))
			return ;
	}
	else
		ft_putchar_fd(b[0]);
}

static void					ft_signals(void)
{
	signal(SIGTSTP, sighandler);
	signal(SIGWINCH, ft_sigwinch);
}

int							main(int ac, char **av)
{
	t_list					*hd;
	int						max;
	char					b[1];

	hd = NULL;
	max = display_list(hd);
	if ((ac == 1) || (strncmp(av[1], "", 1) == 0))
		return (0);
	ft_create_list(av, &hd);
	init_config();
	line_no(1, 's');
	tputs(tgetstr("vi", NULL), 1, ft_putchar_fd);
	ft_signals();
	while (1)
	{
		set_list(&hd, 's');
		read(0, b, 1);
		ft_key(max, &b[0]);
		hd = ft_space(hd, max, &b[0]);
		tputs(tgetstr("cl", NULL), 1, ft_putchar_fd);
		max = display_list(hd);
	}
	ft_exit(0, NULL);
	return (0);
}
