/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_lists.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:11:07 by arangari          #+#    #+#             */
/*   Updated: 2018/05/23 17:24:48 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void						display_item(t_list *hd)
{
	if (hd->active)
		tputs(tgetstr("mr", NULL), 1, ft_putchar_fd);
	if (hd->id == line_no(8, 'g'))
	{
		tputs(tgetstr("us", NULL), 1, ft_putchar_fd);
		ft_putstr_fd(hd->name);
		tputs(tgetstr("ue", NULL), 1, ft_putchar_fd);
	}
	else
		ft_putstr_fd(hd->name);
	if (hd->active)
		tputs(tgetstr("me", NULL), 1, ft_putchar_fd);
	if (hd->next)
		ft_putchar_fd(' ');
}

static void					ft_term(t_list *hd, struct winsize ts)
{
	int						ac_ln;

	ac_ln = 0;
	if (hd->id == 1)
		display_item(hd);
	else
	{
		if ((ac_ln + ft_strlen(hd->name)) <= (unsigned)(ts.ws_col - 9))
		{
			ac_ln += ft_strlen(hd->name);
			ft_putstr_fd(" ");
		}
		else
		{
			ac_ln = 0;
			ft_putchar_fd('\n');
		}
		display_item(hd);
	}
}

int							display_list(t_list *hd)
{
	int						count_max;
	struct winsize			ts;

	count_max = 0;
	ioctl(0, TIOCGWINSZ, &ts);
	tputs(tgetstr("cl", NULL), 1, ft_putchar_fd);
	if (ft_max_length(0, 'g') < (ts.ws_col - 5))
	{
		while (hd)
		{
			ft_term(hd, ts);
			hd = hd->next;
			count_max++;
		}
	}
	else
	{
		ft_putstr_fd("Error");
	}
	return (count_max);
}

int							active_node(t_list **hd, int line_n, int max)
{
	t_list					*p_hd;

	p_hd = *hd;
	while (p_hd)
	{
		if (p_hd->id == line_n)
		{
			if (p_hd->active)
				p_hd->active = false;
			else
				p_hd->active = true;
		}
		p_hd = p_hd->next;
	}
	if (line_n != max)
		line_no(line_n + 1, 's');
	else
		line_no(1, 's');
	return (1);
}
