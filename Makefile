# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: arangari <arangari@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/03 11:42:20 by arangari          #+#    #+#              #
#    Updated: 2018/05/23 17:27:52 by arangari         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_select

CC = gcc -g

CFLAGS = -Wall -Werror -Wextra

SRC  = ft_select.c ft_canonmode.c ft_display_lists.c ft_global.c \
		 ft_lists.c ft_movement.c ft_signals.c

OBJ = $(SRC:%.c=%.o)

all:$(NAME)

$(NAME): $(OBJ)
	@$(CC) -o ft_select $(OBJ) -ltermcap

$(OBJ): $(SRC)
	@$(CC) $(CFLAGS) -c $(SRC)


clean:
	@rm -rf $(OBJ)

fclean: clean
	@rm -rf $(NAME)

re:fclean all

.PHONY:all clean fclean re
